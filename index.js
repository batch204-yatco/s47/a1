// Directions:
// Create two event listeners when a user types in the first and last name inputs 
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

// Stretch Goal:
// Instead of an anonymous function, create a new function that the two event listeners will call

// Guide Question: Where do the names come from and where should they go?

let input1 = document.querySelector("#txt-first-name");
let input2 = document.querySelector("#txt-last-name");
let span1 = document.querySelector("#span-full-name");

const updateFullName = () => {
	let firstName = input1.value
	let lastName = input2.value
	span1.innerHTML = `${firstName} ${lastName}`
}

input1.addEventListener('keyup', () => {
	updateFullName()
})


input2.addEventListener('keyup', updateFullName)